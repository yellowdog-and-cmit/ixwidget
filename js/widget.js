// created at 20-01-2021
var apiURLBase = 'https://peeringdb.com/api/';
var nineObj = '';
var facIds = [];
var uniqueFacIds = [];
var ixIds = [];
var uniqueIxIds = [];
var uniqueIxIdsFiltered = [];
var tab1 = [];
var th1 = [];
var thead1Arr = ['Address', 'IX`s @', 'Facilities @', 'IX Name:'];
var thead1Arr2 = ['', '', '', '#ASNs:'];
var thead2Arr = ['Network Name (ASN)', '#IX`s', 'IX Name:'];
var thead2Arr2 = ['', '', '#ASNs:'];
var tbody1 = '';
var consData1 = [];
var consData2 = [];
var facIXIds = [];
var stringPad = 100;
var strforpad = '0';
var pageIdGlobal = '';
var latetudeLTEGlobal;
var latetudeGTEGlobal;
var logitudeLTEGlobal;
var logitudeGTEGlobal;
var cookieDays = '14';
var rooturlGlobal = 'index.php';
var cookieArr = [];
var tHeadHTMLTot = '';
var tBodyHtmlTot = '';
var tHead2HTMLTot = '';
var tBody2HtmlTot = '';
function stirngPadCalc()
{
    strforpad = new Array(stringPad + 1).join(strforpad);
}
function pad(n) {
    return (strforpad + n).substr(-stringPad);
}
function natural_expand(a) {
    return a.replace(/\d+/g, pad);
}
function GetNinesData(yourUrl) {
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET", yourUrl, false);
    Httpreq.send(null);
    return Httpreq.responseText;
}
function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}
function multiSort(array, sortObject = {}) {
    const sortKeys = Object.keys(sortObject);
    // Return array if no sort object is supplied.
    if (!sortKeys.length) {
        return array;
    }

    // Change the values of the sortObject keys to -1, 0, or 1.
    for (let key in sortObject) {
        sortObject[key] = sortObject[key] === 'desc' || sortObject[key] === -1 ? -1 : (sortObject[key] === 'skip' || sortObject[key] === 0 ? 0 : 1);
    }

    const keySort = (a, b, direction) => {
        direction = direction !== null ? direction : 1;

        if (a === b) { // If the values are the same, do not switch positions.
            return 0;
        }

        // If b > a, multiply by -1 to get the reverse direction.
        return a > b ? direction : -1 * direction;
    };

    return array.sort((a, b) => {
        let sorted = 0;
        let index = 0;

        // Loop until sorted (-1 or 1) or until the sort keys have been processed.
        while (sorted === 0 && index < sortKeys.length) {
            const key = sortKeys[index];

            if (key) {
                const direction = sortObject[key];

                sorted = keySort(a[key], b[key], direction);
                index++;
            }
        }

        return sorted;
    });
}
//-----------------------------//
function getnines(latetudeLTE, latetudeGTE, longitudeLTE, longitudeGTE, pageId, rooturl)
{
    pageIdGlobal = pageId;
    rooturlGlobal = rooturl;
    latetudeLTEGlobal = latetudeLTE;
    latetudeGTEGlobal = latetudeGTE;
    logitudeLTEGlobal = longitudeLTE;
    logitudeGTEGlobal = longitudeGTE;
    executeNines();
}
function executeNines()
{
    stirngPadCalc();
    var urlCreate = apiURLBase + 'ixfac?fac__latitude__lte=' + latetudeLTEGlobal + '&fac__latitude__gte=' + latetudeGTEGlobal + '&fac__longitude__gte=' + logitudeGTEGlobal + '&fac__longitude__lte=' + logitudeLTEGlobal;
    var nineData = JSON.parse(GetNinesData(urlCreate));
    nineObj = nineData.data;
    getNinesData();
}
function getNinesData()
{
    nineObj.forEach(function (obj) {
        facIds.push(obj.fac_id);
        ixIds.push(obj.ix_id);
        checkfacIXIds(obj.fac_id, obj.ix_id);
    });
    uniqueFacIds = facIds.filter(onlyUnique);
    uniqueIxIds = ixIds.filter(onlyUnique);
    createNetIds();
    createtabOneobj();
}
function checkfacIXIds(fac_id, ix_id)
{
    var insFlag = 0;
    if (facIXIds.length == 0)
    {
        insFlag = 1;

    } else
    {
        insFlag = 1;
        facIXIds.forEach(function (obj) {
            if (fac_id == obj.fac_id)
            {
                insFlag = 0;
                obj.ix_id.push(ix_id);
            }
        });
    }
    if (insFlag == 1)
    {
        facIXIds.push({
            "fac_id": fac_id,
            "ix_id": [ix_id]
        });
    }
}
function createNetIds()
{
    for (var i = 0; i < uniqueIxIds.length; i++)
    {
        var netArray = [];
        var netNameArr = [];
        var createaddressurl = apiURLBase + 'netixlan?ix_id=' + uniqueIxIds[i];
        var netData = JSON.parse(GetNinesData(createaddressurl));
        var netObj = netData.data;
        if (netObj[0])
        {
            var netName = sanitizeIXname(netObj[0].name);
            netObj.forEach(function (obj) {
                netArray.push(obj.net_id);
                createtabtwoObj(obj.net_id, uniqueIxIds[i]);
            });
            var uniqueNetArr = netArray.filter(onlyUnique);
            th1.push({
                "ix_id": uniqueIxIds[i],
                "netCount": uniqueNetArr.length,
                "netName": netName,
                "s_netName": netName.toLowerCase()
            });
        } else
        {
            var createaddressurlth = apiURLBase + 'ix?id=' + uniqueIxIds[i];
            var ixData = JSON.parse(GetNinesData(createaddressurlth));
            var ixObj = ixData.data;
            var netName = sanitizeIXname(ixObj[0].name);
            th1.push({
                "ix_id": uniqueIxIds[i],
                "netCount": 0,
                "netName": netName,
                "s_netName": netName.toLowerCase()
            });
        }
    }
    multiSort(th1, {
        netCount: 'desc',
        s_netName: 'asc'
    });
    createtabOneThobj();
    createtabTwoThobj();
}
function createtabOneThobj()
{
    var tHeadHTML = '<tr>';
    var tHeadHTML2 = '<tr>';
    for (var i = 0; i < thead1Arr.length; i++)
    {
        tHeadHTML += '<th>' + thead1Arr[i] + '</th>';
        tHeadHTML2 += '<th>' + thead1Arr2[i] + '</th>';
    }
    th1.forEach(function (obj) {
        var thLink = createTHlink(obj.ix_id, obj.netName);
        tHeadHTML += '<th>' + thLink + '</th>';
        tHeadHTML2 += '<th><span style="color:#666">' + obj.netCount + '</span></th>';
    });
    tHeadHTML += '</tr>';
    tHeadHTML2 += '</tr>';
    tHeadHTMLTot = tHeadHTML + tHeadHTML2;
}
function createtabTwoThobj()
{
    var tHeadHTML = '<tr>';
    var tHeadHTML2 = '<tr>';
    for (var i = 0; i < thead2Arr.length; i++)
    {
        tHeadHTML += '<th>' + thead2Arr[i] + '</th>';
        tHeadHTML2 += '<th>' + thead2Arr2[i] + '</th>';
    }
    th1.forEach(function (obj) {
        var thLink = createTHlink(obj.ix_id, obj.netName);
        tHeadHTML += '<th>' + thLink + '</th>';
        tHeadHTML2 += '<th><span style="color:#666">' + obj.netCount + '</span></th>';
    });
    tHeadHTML += '</tr>';
    tHeadHTML2 += '</tr>';
    tHead2HTMLTot = tHeadHTML + tHeadHTML2;
    multiSort(consData2, {
        ixIdsCount:'desc',
				s_net_name: 'asc'
    });
    executetable2();
}
function executetable2()
{
    var tbody2 = '';
    consData2.forEach(function (obj) {
        tbody2 += '<tr>';
        var urlNetId = createNetURL(obj.net_id, obj.net_name);
        tbody2 += '<td>' + urlNetId + ' ('+obj.asn+')</td>';
        tbody2 += '<td>' + obj.ixIdsCount + '</td>';
        tbody2 += '<td class="yd-blankcell"></td>';
        th1.forEach(function (objth) {
            var netName = objth.netName;
            var chkNet = chkNew1(objth.ix_id, obj.ix_ids);
            tbody2 += '<td align="center" title="' + netName + '" rel="tooltip">' + chkNet + '</td>';
        });
        tbody2 += '</tr>';
    });
    tBody2HtmlTot = tbody2;
}
function createtabtwoObj(netId, ixid)
{
    var createaddressurl = apiURLBase + 'net?id=' + netId;
    var networkData = JSON.parse(GetNinesData(createaddressurl));
    var networkObj = networkData.data;
    var networkName = networkObj[0].name;
    var asn = networkObj[0].asn;
    checkNetwork2(netId, networkName, ixid, asn);
}
function checkNetwork2(netId, networkName, ixid, asn)
{
    var insFlag = 0;
    if (consData2.length == 0)
    {
        insFlag = 1;

    } else
    {
        insFlag = 1;
        consData2.forEach(function (obj) {
            if (obj.net_id == netId)
            {
                insFlag = 0;
                var ixIds = obj.ix_ids;
                ixIds.push(ixid);
                var newixIdsFiltered = ixIds.filter(onlyUnique);
                obj.ix_ids = newixIdsFiltered;
                obj.ixIdsCount = newixIdsFiltered.length;
            }
        });
    }
    if (insFlag == 1)
    {
        consData2.push({
            "net_id": netId,
            "asn": asn,
            "net_name": networkName,
            "s_net_name": natural_expand(networkName.toLowerCase()),
            "ix_ids": [ixid],
            "ixIdsCount": 1
        });
    }
}
function createTHlink(id, name)
{
    var urlTh = '<a target="_blank" href="https://www.peeringdb.com/ix/' + id + '">' + name + '</a>';
    return urlTh;
}
function checkAddress(lat, long, address, facId)
{
    var insFlag = 0;
    if (consData1.length == 0)
    {
        insFlag = 1;

    } else
    {
        insFlag = 1;
        consData1.forEach(function (obj) {
            var diffLat = Math.abs(obj.latitude - lat);
            var diffLong = Math.abs(obj.longitude - long);
            if (diffLat < 0.00155 && diffLong < 0.00155)
            {
                insFlag = 0;
                var ixIds = checkIxInFAC(facId);
                var objixIds = obj.ixIds;
                var newixIds = objixIds.concat(ixIds);
                var filteredixIds = filterixIds(newixIds);
                obj.fac_id.push(facId);
                obj.ixIds = filteredixIds;
                obj.ixIdsCount = filteredixIds.length;
            }
        });
    }
    if (insFlag == 1)
    {
        var ixIds = checkIxInFAC(facId);
        var filteredixIds = filterixIds(ixIds);
        consData1.push({
            "fac_id": [facId],
            "address": address,
            "s_address": natural_expand(address.toLowerCase()),
            "latitude": lat,
            "longitude": long,
            "ixIds": filteredixIds,
            "ixIdsCount": filteredixIds.length
        });
    }
}
function filterixIds(ixIds)
{
    ixIds.filter(onlyUnique);

    var filteredArray = uniqueIxIds.filter(function (n) {
        return ixIds.indexOf(n) !== -1;
    });
    return filteredArray;
}
function checkIxInFAC(facId)
{
    var ixIds = [];
    facIXIds.forEach(function (obj) {
        if (obj.fac_id == facId)
        {
            ixIds = obj.ix_id;
        }
    });
    return ixIds;
}
function createtabOneobj()
{
    for (var i = 0; i < uniqueFacIds.length; i++)
    {
        var createaddressurl = apiURLBase + 'fac?id=' + uniqueFacIds[i];
        var addresssData = JSON.parse(GetNinesData(createaddressurl));
        var address = addresssData.data[0].address1;
        var lat = addresssData.data[0].latitude;
        var long = addresssData.data[0].longitude;
        checkAddress(lat, long, address, uniqueFacIds[i]);
    }

    multiSort(consData1, {
        ixIdsCount: 'desc',
        s_address: 'asc'
    });
    executetable1();
}
function executetable1()
{
    var tbody1 = '';
    consData1.forEach(function (obj) {
        tbody1 += '<tr>';
        tbody1 += '<td>' + obj.address + '</td>';
        tbody1 += '<td>' + obj.ixIdsCount + '</td>';
        var urlFccIds = createFacUrl(obj.fac_id);
        tbody1 += '<td>' + urlFccIds + '</td>';
        tbody1 += '<td class="yd-blankcell"></td>';
        th1.forEach(function (objth) {
            var netName = objth.netName;
            var chkNet = chkNew1(objth.ix_id, obj.ixIds);
            tbody1 += '<td align="center" title="' + netName + '" rel="tooltip">' + chkNet + '</td>';
        });
        tbody1 += '</tr>';
    });
    tBodyHtmlTot = tbody1;
    createtable1();
}
function chkNew1(ix_id, ixIds)
{
    var retString = '';
    var n = ixIds.includes(ix_id);
    if (n === true)
    {
        var retString = '&#10004';
    }
    return retString;
}
function createFacUrl(facIds)
{
    var urlarray = [];
    for (var i = 0; i < facIds.length; i++)
    {
        var urlSt = '<a target="_blank" href="https://www.peeringdb.com/fac/' + facIds[i] + '"><i class="fa fa-connectdevelop" aria-hidden="true" title="Facility ID' + facIds[i] + '"></i></a>';
        urlarray.push(urlSt);
    }
    var urlstring = urlarray.toString(", ");
    return urlstring;
}
function createtable1()
{
    var tabhead_1 = document.getElementById("headtab1");
    tabhead_1.innerHTML = tHeadHTMLTot;
    var tabbody_1 = document.getElementById("hbodytab1");
    tabbody_1.innerHTML = tBodyHtmlTot;
    createtable2();
}
function createtable2()
{
    var tabhead_2 = document.getElementById("headtab2");
    tabhead_2.innerHTML = tHead2HTMLTot;
    var tabbody_2 = document.getElementById("hbodytab2");
    tabbody_2.innerHTML = tBody2HtmlTot;
    storeCache();
}
function createNetURL(netId, netName)
{
    var urlNetId = '<a target="_blank" href="https://www.peeringdb.com/net/' + netId + '">' + netName + '</a>';
    return urlNetId;
}
function storeCache()
{
    var xhr = new XMLHttpRequest();
    xhr.open("POST", rooturlGlobal + 'ixwidget/includes/save-cache.php', true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            console.log(this.responseText);
        }
    }
    xhr.send('pageId=' + encodeURIComponent(pageIdGlobal) + '&latetudeLTE=' + encodeURIComponent(latetudeLTEGlobal) + '&latetudeGTE=' + encodeURIComponent(latetudeGTEGlobal) + '&longitudeLTE=' + encodeURIComponent(logitudeLTEGlobal) + '&lognitudeGTE=' + encodeURIComponent(logitudeGTEGlobal) + '&thead1=' + encodeURIComponent(tHeadHTMLTot) + '&tbody1=' + encodeURIComponent(tBodyHtmlTot) + '&thead2=' + encodeURIComponent(tHead2HTMLTot) + '&tbody2=' + encodeURIComponent(tBody2HtmlTot));
}
function sanitizeIXname(str)
{
    var ixNameArr=str.split(":");
    var ixSanitizeName=ixNameArr[0];
    return ixSanitizeName;
}