<?php
$url = $_SERVER['REQUEST_URI'];
$pagebasePath = $_SERVER["SCRIPT_FILENAME"];
$pagebasePathArr = explode('/', $pagebasePath);
$pageName = end($pagebasePathArr);
$pageArr = explode(".", $pageName);
$pageId = $pageArr[0];
//Global Parameters for setting
$caching = 'true';
$thead1 = '';
$tbody1 = '';
$thead2 = '';
$tbody2 = '';
$latetudeLTE = '42.7';
$latetudeGTE = '41.16';
$longitudeLTE = '-87.3';
$longitudeGTE = '-88.3';
$executeCode = 'true';
//Global Parameters for setting
if ($caching == 'true') {
    
    $page = $pageId . ".json";
    if (file_exists("caching/" . $page)) {
        
        $jsonArrCache = file_get_contents("caching/" . $page);
        $jsonArr = json_decode($jsonArrCache, true);
        if ($jsonArr['latetudeLTE'] != '' && $jsonArr['latetudeGTE'] != '' && $jsonArr['longitudeLTE'] != '' && $jsonArr['lognitudeGTE'] != '' && $jsonArr['thead1'] != '' && $jsonArr['tbody1'] != '' && $jsonArr['thead2'] != '' && $jsonArr['tbody2'] != '') {            
            if ($jsonArr['latetudeLTE'] == $latetudeLTE && $jsonArr['latetudeGTE'] == $latetudeGTE && $jsonArr['longitudeLTE'] == $longitudeLTE && $jsonArr['lognitudeGTE'] == $longitudeGTE) {
                $executeCode = false;
                $thead1 = $jsonArr['thead1'];
                $tbody1 = $jsonArr['tbody1'];
                $thead2 = $jsonArr['thead2'];
                $tbody2 = $jsonArr['tbody2'];
            }
        }
    }
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <div>
                <h1>INTERNET EXCHANGES (Table-1)</h1>
                <table id="table1" border=1 CELLSPACING=0>
                    <thead id="headtab1"><?php echo $thead1; ?></thead>
                    <tbody id="hbodytab1"><?php echo $tbody1; ?></tbody>
                </table>
            </div>
        </div>
        <div>
            <div>
                <h1>INTERNET EXCHANGES (Table-2)</h1>
                <table id="table2" border=1 CELLSPACING=0>
                    <thead id="headtab2"><?php echo $thead2; ?></thead>
                    <tbody id="hbodytab2"><?php echo $tbody2; ?></tbody>
                </table>
            </div>
        </div>
        <script src="js/widget.js" type="text/javascript"></script>
        <script>
            var latetudeLTE = '<?php echo $latetudeLTE; ?>';
            var latetudeGTE = '<?php echo $latetudeGTE; ?>';
            var longitudeLTE = '<?php echo $longitudeLTE; ?>';
            var longitudeGTE = '<?php echo $longitudeGTE; ?>';
            var pageId = '<?php echo $pageId; ?>';
            var rooturl = '<?php echo $url; ?>';
            var caching = '<?php echo $caching; ?>';
            var executeCode = '<?php echo $executeCode; ?>';
            if (caching == 'false' || executeCode == 'true')
            {
                getnines(latetudeLTE, latetudeGTE, longitudeLTE, longitudeGTE, pageId, rooturl);
            }
        </script>
    </body>
</html>