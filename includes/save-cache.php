<?php

$pageName = $_POST['pageId'];
$pageArr = explode(".", $pageName);
$pageId = $pageArr[0];
$jsonPageId = $pageId . ".json";
$dataArray = array(
    'latetudeLTE' => $_POST['latetudeLTE'],
    'latetudeGTE' => $_POST['latetudeGTE'],
    'longitudeLTE' => $_POST['longitudeLTE'],
    'lognitudeGTE' => $_POST['lognitudeGTE'],
    'thead1' => $_POST['thead1'],
    'tbody1' => $_POST['tbody1'],
    'thead2' => $_POST['thead2'],
    'tbody2' => $_POST['tbody2']
);
$jsonArr = json_encode($dataArray);
$fp = fopen("../caching/" . $jsonPageId, 'w');
fwrite($fp, $jsonArr);
fclose($fp);
